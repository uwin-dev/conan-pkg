from conans import ConanFile, tools, RunEnvironment
from pathlib import Path
import os
import tempfile

class GccConan(ConanFile):
    name = "x86_64-linux-gnu-gcc-11"
    version = "11.1.0"
    settings = "os", "arch"
    license = "GPLv3"
    build_requires = "crosstool-ng/1.24.0.390_62e9db2@uwin/stable"
    exports_sources = "x86_64-linux-gnu-gcc-11.cmake", "patches/*"
    
    _target_triplet = "x86_64-linux-gnu"
    _toolchain_file_name = f"{_target_triplet}-gcc-11.cmake"

    _COMMON_CONFIG = """
CT_CONFIG_VERSION="3"
CT_ARCH_X86=y
CT_ARCH_64=y
CT_KERNEL_LINUX=y
CT_BINUTILS_LINKER_LD=y
CT_CC_GCC_LNK_HASH_STYLE_BOTH=y
CT_CC_LANG_CXX=y
CT_PREFIX_DIR_RO=n
CT_LOG_PROGRESS_BAR=n
CT_FORBID_DOWNLOAD=y
CT_PATCH_BUNDLED_LOCAL=y
CT_PATCH_ORDER="bundled,local"
CT_PATCH_USE_LOCAL=y
CT_LOCAL_PATCH_DIR="${CT_TOP_DIR}/patches"

CT_LOCAL_TARBALLS_DIR="${CT_TOP_DIR}/downloads"
CT_WORK_DIR="${CT_TOP_DIR}/build"

CT_STRIP_TARGET_TOOLCHAIN_EXECUTABLES=y
CT_OMIT_TARGET_VENDOR=y
CT_TOOLCHAIN_PKGVERSION="uwin"

CT_LINUX_V_3_2=y
CT_LINUX_VERSION="3.2.101"
CT_GLIBC_V_2_25=y
CT_GLIBC_VERSION="2.25"
CT_GLIBC_ENABLE_COMMON_FLAG=y
CT_GLIBC_KERNEL_VERSION_NONE=y
CT_GLIBC_EXTRA_CFLAGS=""

CT_CC_GCC_LIBSANITIZER=y

CT_COMP_LIBS_EXPAT=y
CT_COMP_LIBS_LIBELF=y
"""
    _BOOTSTRAP_CONFIG = """
CT_PREFIX_DIR="${CT_TOP_DIR}/toolchain-bootstrap"
    """
    _REAL_CONFIG = """
CT_BUILD="x86_64-linux-gnu"

CT_PREFIX_DIR="${CT_TOP_DIR}/toolchain-real"
    """
    
    def source(self):
        tools.mkdir("downloads")
        
        tarballs = [
            ("http://ftpmirror.gnu.org/binutils/binutils-2.36.1.tar.xz", "binutils-2.36.1.tar.xz", "e81d9edf373f193af428a0f256674aea62a9d74dfe93f65192d4eae030b0f3b0"),
            ("http://ftpmirror.gnu.org/gcc/gcc-11.1.0/gcc-11.1.0.tar.xz", "gcc-11.1.0.tar.xz", "4c4a6fb8a8396059241c2e674b85b351c26a5d678274007f076957afa1cc9ddf"),
            ("http://ftpmirror.gnu.org/gettext/gettext-0.20.1.tar.xz", "gettext-0.20.1.tar.xz", "53f02fbbec9e798b0faaf7c73272f83608e835c6288dd58be6c9bb54624a3800"),
            ("http://ftpmirror.gnu.org/glibc/glibc-2.25.tar.xz", "glibc-2.25.tar.xz", "067bd9bb3390e79aa45911537d13c3721f1d9d3769931a30c2681bfee66f23a0"),
            ("http://ftpmirror.gnu.org/gmp/gmp-6.2.1.tar.xz", "gmp-6.2.1.tar.xz", "fd4829912cddd12f84181c3451cc752be224643e87fac497b69edddadc49b4f2"),
            ("http://isl.gforge.inria.fr/isl-0.24.tar.xz", "isl-0.24.tar.xz", "043105cc544f416b48736fff8caf077fb0663a717d06b1113f16e391ac99ebad"),
            ("http://ftpmirror.gnu.org/libiconv/libiconv-1.16.tar.gz", "libiconv-1.16.tar.gz", "e6a1b1b589654277ee790cce3734f07876ac4ccfaecbee8afa0b649cf529cc04"),
            ("http://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.2.101.tar.xz", "linux-3.2.101.tar.xz", "4453686b001685144f44c88d57c716fcb6e85ef8a2aad2f95d36df82fa972c59"),
            ("http://ftpmirror.gnu.org/mpc/mpc-1.2.0.tar.gz", "mpc-1.2.0.tar.gz", "e90f2d99553a9c19911abdb4305bf8217106a957e3994436428572c8dfe8fda6"),
            ("http://ftpmirror.gnu.org/mpfr/mpfr-4.1.0.tar.xz", "mpfr-4.1.0.tar.xz", "0c98a3f1732ff6ca4ea690552079da9c597872d30e96ec28414ee23c95558a7f"),
            ("http://ftpmirror.gnu.org/ncurses/ncurses-6.2.tar.gz", "ncurses-6.2.tar.gz", "30306e0c76e0f9f1f0de987cf1c82a5c21e1ce6568b9227f7da5b71cbea86c9d"),
            ("https://zlib.net/zlib-1.2.11.tar.xz", "zlib-1.2.11.tar.xz", "4ff941449631ace0d4d203e3483be9dbc9da454084111f97ea0a2114e19bf066")
        ]

        for url, filename, sha256 in tarballs:
            tools.download(url, Path('downloads') / filename, sha256=sha256)
        
        #tools.save("defconfig", self._COMMON_CONFIG + self._BOOTSTRAP_CONFIG)
        #with tools.environment_append({"CC": None, "CXX": None}):
        #    self.run("CT_VCHECK=load ct-ng defconfig source")

    def build(self):
        # the idea is to build a toolchain using an old glibc so that the binaries they build can work across different linux distros
        # but wait, we want the toolchain to work on those distros too!
        # that is why we build two toolchains:
        # the first one using our host compiler targeting the old glibc
        # and the second one using the previously built one
        # that way we get a toolchain that can both run and target most glibc-based linux distros
        
        # and another thing: in gitlab ci the path to the build directory becomes too long, leading to an error "execvp: /bin/bash: Argument list too long"
        # that's why we create a temp directory, put there the symlink to actual build directory and build there
        # (Assuming the generated temp path will be shorted ¯\_(ツ)_/¯)
        
        with tempfile.TemporaryDirectory() as td:
            short_build_folder = str(Path(td) / 'bld')
            os.makedirs(short_build_folder, exist_ok=True)
            self.run(['bindfs', '--no-allow-other', self.build_folder, short_build_folder])
            try:
                with tools.chdir(short_build_folder):
                    self.output.info(f"Using {short_build_folder} as a shorter build directory alias")
                    self.output.info(os.getcwd())
                    self.run('pwd')
                    
                    self.output.info("Building the bootstrap toolchain")
                    tools.save("defconfig", self._COMMON_CONFIG + self._BOOTSTRAP_CONFIG)
                    with tools.environment_append({"CC": None, "CXX": None}):
                        # build bootstrap toolchain
                        self.run("CT_VCHECK=load ct-ng defconfig")
                        try:
                            self.run("ct-ng build")
                        except:
                            self.run("cat build.log")
                            raise

                    bootstrap_bin = self.build_folder + "/toolchain-bootstrap/bin"
                    self.run([bootstrap_bin + f"/{self._target_triplet}-g++", "--version"])
                    
                    self.output.info("Building the real toolchain with the bootstrap one")
                    tools.save("defconfig", self._COMMON_CONFIG + self._REAL_CONFIG)
                    with tools.environment_append({"CC": None, "CXX": None, "PATH": bootstrap_bin + ":" + tools.get_env('PATH')}):
                        # build bootstrap toolchain
                        self.run("CT_VCHECK=load ct-ng defconfig")
                        try:
                            self.run("ct-ng build")
                        except:
                            self.run("cat build.log")
                            raise
            finally:
                self.run(['fusermount', '-zu', short_build_folder])

    def package(self):
        self.copy("*", src="toolchain-real")
        self.copy(self._toolchain_file_name, dst="build/cmake")

    def _define_tool_var(self, name, value):
        pkg = Path(self.package_folder)
        path = pkg / 'bin' / f"{self._target_triplet}-{value}"
        if not path.is_file():
            self.output.error(f"A required tool is expected to exist: {name} at {path}")
        self.output.info(f"Creating {name} environment variable: {path}")
        
        setattr(self.env_info, name, str(path))

    def package_info(self):
        pkg = Path(self.package_folder)
        self.output.info("setting environment (pkg = %s)" % str(pkg))

        self.env_info.PATH.append(str(pkg / 'bin'))
        self.env_info.CONAN_CMAKE_TOOLCHAIN_FILE = str(pkg / 'build/cmake' / self._toolchain_file_name)
        
        self._define_tool_var('CC', 'gcc')
        self._define_tool_var('CXX', 'g++')
        self._define_tool_var('LD', 'ld')
        self._define_tool_var('AR', 'ar')
        self._define_tool_var('AS', 'as')
        self._define_tool_var('RANLIB', 'ranlib')
        self._define_tool_var('STRIP', 'strip')
        self._define_tool_var('ADDR2LINE', 'addr2line')
        self._define_tool_var('NM', 'nm')
        self._define_tool_var('OBJCOPY', 'objcopy')
        self._define_tool_var('OBJDUMP', 'objdump')
        self._define_tool_var('READELF', 'readelf')
        self._define_tool_var('ELFEDIT', 'elfedit')



