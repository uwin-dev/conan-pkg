from conans import ConanFile, CMake, tools, RunEnvironment
from pathlib import Path
import os

class GhidraConan(ConanFile):
    name = "ghidra"
    version = "9.2.3"
    settings = ()
    url = "https://ghidra-sre.org/"
    license = "Apache"
    description = """Software reverse engineering framework"""
    

    def build(self):
        url = ("https://ghidra-sre.org/ghidra_%s_PUBLIC_20210325.zip"
                % (str(self.version)))
        tools.get(url, strip_root=True, sha256='9019c78f8b38d68fd40908b040466974a370e26ba51b2aaeafc744171040f714', keep_permissions=True)

    def package(self):
        self.copy("*")

    def package_info(self):
        ghidra = Path(self.package_folder)
        self.output.info("setting environment (ghidra = %s)" % str(ghidra))

        self.env_info.GHIDRA = str(ghidra)
    
