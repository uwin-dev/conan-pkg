from conans import ConanFile, CMake, tools, RunEnvironment
from pathlib import Path

class OpenWatcomV2Conan(ConanFile):
    name = "openwatcom-v2"
    version = "2021-04-02"
    settings = "os", "arch"
    url = "https://github.com/open-watcom"
    license = "Watcom-1.0"
    description = """The Open Watcom Fortran/C/C++ compiler, binary distribution -V2 fork"""
    

    def build(self):
        if self.settings.os != "Linux" or self.settings.arch != "x86_64":
            raise Exception("Binary does not exist for these settings")
        url = ("https://github.com/open-watcom/open-watcom-v2/releases/download/%s-Build/ow-snapshot.tar.gz"
                % (str(self.version)))
        tools.get(url, sha256='f4cf8f744f5602a6cb632bc0462a5b7440fe0d99e9132d64580d53b10ab93f27')

    def package(self):
        self.copy("*")

    def package_info(self):
        watcom = Path(self.package_folder)
        self.output.info("setting environment (watcom = %s)" % str(watcom))

        self.env_info.WATCOM = str(watcom)
        self.env_info.PATH.append(str(watcom / "binl64"))
        self.env_info.EDPATH = str(watcom / "eddat")
        self.env_info.WIPFC = str(watcom / "wipfc")
    
