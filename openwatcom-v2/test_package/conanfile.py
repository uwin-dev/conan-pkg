from conans import ConanFile, CMake, tools, RunEnvironment
import os
from pathlib import Path

class TestPackageConan(ConanFile):
    settings = "os", "build_type", "arch", "compiler"    

    def test(self):
        self.output.info("cwd = %s" % os.getcwd())
        self.output.info("source_folder = %s" % self.source_folder)

        watcom = os.getenv("WATCOM")
        src = Path(self.source_folder)
        
        self.run("mkdir -p build")
        self.run(["env", "INCLUDE=%s/h:%s/h/nt" % (watcom, watcom), "owcc", "-bwin95", str(src / "hello_world.c"), "-o", "hello_world.exe"], run_environment=True)
        self.run(["file", "hello_world.exe"])
        #self.run(f"INCLUDE=$WATCOM/h:$WATCOM/h/nt wcc386 -bt=nt -hd -d3 -bc hello_world.c -fo=build/hello_world.o", run_environment=True)
        #self.run("wlink < %s/hello_world.lnk" % str(src), run_environment=True)
        self.run("echo")
        self.run("echo WATCOM=$WATCOM")
