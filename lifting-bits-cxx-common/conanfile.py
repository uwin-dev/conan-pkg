from conans import ConanFile, CMake, tools, RunEnvironment
from pathlib import Path

class LiftingBitsCxxCommonConan(ConanFile):
    name = "lifting-bits-cxx-common"
    version = "0.1.4"
    settings = "os", "arch"
    url = "https://github.com/trailofbits/cxx-common"
    license = "Watcom-1.0"
    description = """Common dependency management for various Trail of Bits C++ codebases """

    def source(self):
        git = tools.Git()
        git.clone("https://github.com/trailofbits/cxx-common.git", "f73917cc66ea6521b686cc8b2ae77374a3084b7c", shallow=True)
    
    def configure(self):
        if self.settings.os != "Linux" or self.settings.arch != "x86_64":
            raise Exception("Only Linux on x86_64 is supported for now")

    def build(self):
        self.run("./build_dependencies.sh --export-dir vcpkg_root --release llvm-11")

    def package(self):
        self.copy("*", ".", "vcpkg_root")
