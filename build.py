import os
import shutil

import yaml
import argparse
from pathlib import Path

from conans.client import conan_api
from conans.client.conan_api import ProfileData
from conans.client.remote_manager import RemoteManager
from conans.errors import NotFoundException
from conans.model.ref import get_reference_fields, ConanFileReference

import sys


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


# TODO: consider making the conan non-interactive
conan = conan_api.Conan()

packages_root = yaml.load(open("packages.yml"), Loader=yaml.FullLoader)
packages = packages_root['packages']
images = packages_root['image-aliases']


def resolve_image(image):
    if image in images:
        return resolve_image(images[image])
    return image


def build(args):
    REFERENCE = args.reference  # "uwin/stable"
    NAME, VERSION, USER, CHANNEL, _ = get_reference_fields(REFERENCE, user_channel_input=True)

    if any([NAME, VERSION]) or not all([USER, CHANNEL]):
        raise RuntimeError("Invalid parameter '%s', specify the user/channel reference" % REFERENCE)

    CREATE_ADDITIONAL_KWARGS = dict()
    if args.no_test:
        CREATE_ADDITIONAL_KWARGS.update({'test_folder': False})

    selected_packages = args.packages or packages.keys()

    known_packages = set()

    for package_name in packages.keys():
        conanfile = app.loader.load_named(str(Path(os.getcwd()) / package_name / "conanfile.py"), None, None, USER,
                                          CHANNEL)

        version = conanfile.version
        actual_package_name = conanfile.name

        if actual_package_name != package_name:
            raise RuntimeError(f"Mismatch between directory and package names: directory is {package_name},"
                               f" while package is {actual_package_name}")

        full_reference = f"{package_name}/{version}@{REFERENCE}"

        known_packages.add(full_reference)

    def prune_cache_for(ref):
        if args.only_use_in_ci_prune_cache:
            eprint("cleaning up (only removing sources, build directories) for", ref)
            conan.remove(ref, src=[], builds=[], force=True)

    for package_name in selected_packages:
        eprint(f"Taking a look at {package_name}...")

        package = packages[package_name]
        conanfile = app.loader.load_named(str(Path(os.getcwd()) / package_name / "conanfile.py"), None, None, USER,
                                          CHANNEL)

        ref = ConanFileReference(conanfile.name, conanfile.version, USER, CHANNEL)
        # data = conan.inspect(package_name, ['name', 'version'])

        version = conanfile.version
        full_reference = str(ref)  # f"{package_name}/{version}@{REFERENCE}"

        conan.export(package_name, package_name, version, USER, CHANNEL)

        layout = app.cache.package_layout(ref)

        outdated = args.force_outdated

        if not outdated:

            local_revision = layout.recipe_revision()
            # local_revision = conan.get_recipe_revisions(full_reference)
            try:
                # remote_revisions = conan.get_recipe_revisions(full_reference, remote_name=REMOTE)
                remote_revisions = {x['revision'] for x in remote_manager.get_recipe_revisions(ref, remote)}
            except NotFoundException:
                remote_revisions = set()
                pass

            rref = ConanFileReference(ref.name, ref.version, ref.user, ref.channel, local_revision)

            if local_revision not in remote_revisions:
                eprint("Local revision not found at remote, marking package as outdated")
                outdated = True
        # here we assume that the recipe will not be uploaded to remote unless all the profiles have succeeded building
        # this may not be necessarily true, so we need to make sure this logic is maintained

        if outdated:
            eprint(package_name, "is outdated, building...")
            for profile in package['profiles']:
                eprint("building", package_name, "with", profile)
                conan.create(package_name, name=package_name, version=version, user=USER, channel=CHANNEL,
                             profile_names=[profile], build_modes=['outdated'], remote_name=REMOTE,
                             **CREATE_ADDITIONAL_KWARGS)

            if not args.no_upload:
                conan.upload(full_reference, remote_name=REMOTE, all_packages=True, retry=10)
        else:
            eprint(package_name, "seems up-to-date, making sure all the binary packages are present")
            for profile in package['profiles']:
                eprint("checking", package_name, "with", profile)
                conan.create(package_name, name=package_name, version=version, user=USER, channel=CHANNEL,
                             profile_names=[profile], build_modes=['never'], remote_name=REMOTE,
                             test_folder=False)
            # in case we have the package in local cache but not on the remote
            if not args.no_upload:
                conan.upload(full_reference, remote_name=REMOTE, all_packages=True, retry=10)

    if args.only_use_in_ci_prune_cache:
        # the algorithm is as follows:
        # traverse all packages in the local cache
        # delete all the ones that are not mentioned in the packages.yml or do not match the versions in the repo

        search_results = conan.search_recipes('*')['results'][0]['items']

        for x in search_results:
            ref = x['recipe']['id']
            if ref not in known_packages:
                eprint("removing", ref)
                conan.remove(ref, force=True)


def format_job(reference, stage, artifacts_url):
    name, version, user, channel, revision = get_reference_fields(reference, user_channel_input=True)
    package = packages[name]
    return f"""
build-{name}:
    stage: stage-{stage}
    image: {resolve_image(package.get('image', 'default'))}
    variables:
        CONAN_USER_HOME: "/var/lib/conan-home"
        CONAN_LOGIN_USERNAME: "uwin_ci"
        CONAN_PASSWORD: "${{UWIN_CONAN_CI_PASSWORD}}"
    script:
        - wget {artifacts_url} -O artifacts.zip
        - unzip artifacts.zip
        - rm artifacts.zip
        - conan config set storage.download_cache="/var/cache/conan"
        - conan config install https://gitlab.com/uwin-dev/conan-config.git
        - conan user uwin_ci -p ${{CONAN_PASSWORD}} -r uwin-build
        - python3 -u build.py execute-job {reference}
"""


def prepare_jobs(args):
    REFERENCE = args.reference  # "uwin/stable"
    NAME, VERSION, USER, CHANNEL, _ = get_reference_fields(REFERENCE, user_channel_input=True)

    cwd = Path(os.getcwd())
    lockfiles_path = cwd / 'locks'
    if lockfiles_path.exists():
        shutil.rmtree(lockfiles_path)

    for package_name in packages.keys():
        conanfile_path = str(cwd / package_name / "conanfile.py")

        conanfile = app.loader.load_named(str(conanfile_path), None, None, USER, CHANNEL)

        version = conanfile.version
        actual_package_name = conanfile.name

        full_reference = f"{package_name}/{version}@{REFERENCE}"

        if actual_package_name != package_name:
            raise RuntimeError(f"Mismatch between directory and package names: directory is {package_name},"
                               f" while package is {actual_package_name}")

        eprint(f"Exporting & uploading {package_name}...")

        # export and upload the recipe to be consumed by later jobs
        conan.export(package_name, package_name, version, USER, CHANNEL)
        conan.upload(full_reference, remote_name=REMOTE)

    lockfiles = []

    CREATE_ADDITIONAL_KWARGS = dict()

    for package_name in packages.keys():
        package = packages[package_name]

        pkg_lockfiles_path = lockfiles_path / package_name
        conanfile_path = str(cwd / package_name / "conanfile.py")
        os.makedirs(pkg_lockfiles_path, exist_ok=True)

        eprint(f"Locking {package_name}...")
        conanfile = app.loader.load_named(conanfile_path, None, None, USER, CHANNEL)

        version = conanfile.version
        full_reference = f"{package_name}/{version}@{REFERENCE}"

        base_lock_path = str(pkg_lockfiles_path / 'base.lock')
        conan.lock_create(None, base_lock_path, reference=full_reference, base=True,
                          user=USER, channel=CHANNEL, remote_name=REMOTE,
                          build=['outdated'] if not args.force_outdated else [], **CREATE_ADDITIONAL_KWARGS)
        for profile in package['profiles']:
            profile_host = ProfileData([profile], None, None, None, None)

            profile_lock_path = str(pkg_lockfiles_path / f'{profile}.lock')
            conan.lock_create(None, profile_lock_path, reference=full_reference, profile_host=profile_host,
                              lockfile=base_lock_path, user=USER, channel=CHANNEL, remote_name=REMOTE,
                              build=['outdated'] if not args.force_outdated else [], **CREATE_ADDITIONAL_KWARGS)
            lockfiles.append(profile_lock_path)

    eprint("Creating lockfile bundle...")
    lock_bundle_path = str(lockfiles_path / 'lock.bundle')
    conan.lock_bundle_create(lockfiles, lock_bundle_path)

    build_order = conan.lock_bundle_build_order(lock_bundle_path)

    resulting_yaml = ""
    if len(build_order):
        resulting_yaml += "stages:\n"
        for i in range(1, len(build_order) + 1):
            resulting_yaml += f" - stage-{i}\n"
        resulting_yaml += "\n"

    for stage, refs in enumerate(build_order):
        for ref in refs:
            resulting_yaml += format_job(ref, stage + 1, args.artifacts_url)

    with open(args.generated_config, 'w') as f:
        f.write(resulting_yaml)


def execute_job(args):
    name, version, user, channel, rrev = get_reference_fields(args.reference, user_channel_input=True)

    package = packages[name]

    cwd = Path(os.getcwd())
    pkg_lockfiles_path = cwd / 'locks' / name

    for profile in package['profiles']:
        profile_lock_path = str(pkg_lockfiles_path / f'{profile}.lock')
        ref = ConanFileReference.loads(args.reference, validate=False)
        conan.install_reference(ref, build=[args.reference], profile_names=[profile], lockfile=profile_lock_path)
        conan.upload(args.reference, remote_name=REMOTE, all_packages=True)


parser = argparse.ArgumentParser()

subparsers = parser.add_subparsers()

build_parser = subparsers.add_parser('build')

build_parser.add_argument("--remote", "-r", default="uwin",
                          help="specify a conan remote to work with to")

build_parser.add_argument("--no-test", action='store_true',
                          help="Do not test the built packages")
build_parser.add_argument("--no-upload", action='store_true',
                          help="Do not upload the built packages")
build_parser.add_argument("--force-outdated", action='store_true',
                          help="Mark all packages as outdated. Forces the rebuild & upload.")
build_parser.add_argument("--only-use-in-ci-prune-cache", action='store_true',
                          help="[!] MAY BE DESTRUCTIVE IN NON-CI ENVIRONMENT [!] "
                               "Remove everything from conan cache not necessary for "
                               "the ci incremental builds to function")

build_parser.add_argument("reference", help="user/channel reference to build all packages with")
build_parser.add_argument("packages", nargs='*', help="Restrict packages built to these")
build_parser.set_defaults(func=build)

prepare_jobs_parser = subparsers.add_parser('prepare-jobs')

prepare_jobs_parser.add_argument("--remote", "-r", default="uwin-build",
                                 help="specify a conan remote to work with to")

prepare_jobs_parser.add_argument("--force-outdated", action='store_true',
                                 help="Mark all packages as outdated. Forces the rebuild & upload.")

prepare_jobs_parser.add_argument("reference", help="user/channel reference to lock all packages with")
prepare_jobs_parser.add_argument("generated_config", help="path to output gitlab pipeline definition")
prepare_jobs_parser.add_argument("artifacts_url", help="url of the current job artifacts to download in child jobs"
                                                       " (to pass the lockfiles to them)")
prepare_jobs_parser.set_defaults(func=prepare_jobs)

execute_jobs_parser = subparsers.add_parser('execute-job')

execute_jobs_parser.add_argument("--remote", "-r", default="uwin-build",
                                 help="specify a conan remote to work with to")

execute_jobs_parser.add_argument("reference", help="full recipe reference to build")
execute_jobs_parser.set_defaults(func=execute_job)

args = parser.parse_args()

REMOTE = args.remote

conan.create_app()
app: conan_api.ConanApp = conan.app
remote_manager: RemoteManager = app.remote_manager
remote = app.load_remotes(REMOTE).get_remote(REMOTE)

if not app.config.revisions_enabled:
    raise RuntimeError("The client doesn't have the revisions feature enabled."
                       " Enable this feature setting to '1' the environment variable"
                       " 'CONAN_REVISIONS_ENABLED' or the config value"
                       " 'general.revisions_enabled' in your conan.conf file")

args.func(args)
