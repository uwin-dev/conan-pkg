from conans import ConanFile, tools
from conans.errors import ConanInvalidConfiguration
import os


class DiConan(ConanFile):
    name = "di"
    version = "1.2.0"
    license = "BSL-1.0"
    homepage = "https://github.com/boost-ext/di"
    url = "https://github.com/conan-io/conan-center-index"
    description = "DI: C++14 Dependency Injection Library."
    topics = ("dependency-injection", "metaprogramming", "design-patterns")
    exports_sources = ["BSL-1.0.txt"]
    settings = ()
    options = {"with_extensions": [True, False], "diagnostics_level": [0, 1, 2]}
    default_options = {"with_extensions": True, "diagnostics_level": 1}
    exports_sources = ["patches/**"]

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def build(self):
        tools.patch(patch_file="patches/expose-cfg.patch", base_path=self._source_subfolder)

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        extracted_dir = "di-" + self.version
        os.rename(extracted_dir, self._source_subfolder)

    def package(self):
        self.copy("BSL-1.0.txt", src="", dst="licenses")
        if self.options.with_extensions:
            self.copy("*.hpp", src=os.path.join(self._source_subfolder, "extension", "include", "boost", "di", "extension"), dst=os.path.join("include", "boost", "di", "extension"), keep_path=True)
        self.copy("di.hpp", src=os.path.join(self._source_subfolder, "include", "boost"), dst=os.path.join("include", "boost"))

    def package_id(self):
        self.info.requires.clear()
        self.info.settings.clear()
        del self.info.options.diagnostics_level

    def package_info(self):
        self.cpp_info.defines.append("BOOST_DI_CFG_DIAGNOSTICS_LEVEL={}".format(self.options.diagnostics_level))
