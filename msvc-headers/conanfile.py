from conans import ConanFile, tools, AutoToolsBuildEnvironment

import shutil
import os

class MSVCHeaders(ConanFile):
    name = "msvc-headers"
    version = "6.0"
    settings = ()
    no_copy_source = True
    

    def source(self):
        tools.download(url="https://winworldpc.com/download/0cc395c2-b7c3-99c3-89c3-b811c3a4efbf/from/c39ac2af-c381-c2bf-1b25-11c3a4e284a2", sha256="d6c3238d482a4e0665365dd4e2145168726ec7990f34b50b6e885596951eb355",
                  filename="msvc6.7z")
        
    def _mvlower(self, root, names):
        for filename in names:
            newname = filename.lower()
            if newname != filename:
                os.rename(os.path.join(root, filename), os.path.join(root, newname))
        
    def build(self):
        self.run(['7z', 'x', self.source_folder + '/msvc6.7z', '-y', 'VSE600ENU1.ISO'])
        self.run(['7z', 'x', 'VSE600ENU1.ISO', 'VC98/INCLUDE', '-y', '-oVSE600ENU1'])
        os.remove('VSE600ENU1.ISO')
        shutil.rmtree('include', ignore_errors=True)
        shutil.move('VSE600ENU1/VC98/INCLUDE/', 'include/')
        shutil.rmtree('VSE600ENU1')
        
        # lowercase all the files
        for root, folders, files in os.walk('include'):
            self._mvlower(root, files)
        for root, folders, files in os.walk('include'):
            self._mvlower(root, folders)
            
    def package(self):
        self.copy("*", 'include', 'include')
            
    def package_info(self):
        pass
