from conans import ConanFile, AutoToolsBuildEnvironment, tools
from pathlib import Path
import os

class CrosstoolNGFile(ConanFile):
    name = 'crosstool-ng'
    revision = '62e9db247be34f8a4fa3bc116e60a1b15db62a97'
    version = '1.24.0.390_' + revision[:7]
    description = "A versatile (cross-)toolchain generator. "
    url = "https://crosstool-ng.github.io"
    license = "GPLv2"
    
    scm = {
        "type": "git",
        "url": "https://github.com/crosstool-ng/crosstool-ng.git",
        "revision": revision
     }

    def source(self):
        tools.save(".tarball-version", self.version)
    
    def build(self):
        self.run("./bootstrap")
        
        # here we don't use the make install, but crosstool-NG option --enable-local
        # this is to allow the moving of crosstool-NG to other machines
        # (make install hard-codes some paths, while --enable-local does not =))
        # ((we end up installing some junk though))
        autotools = AutoToolsBuildEnvironment(self)
        autotools.configure(args=['--enable-local'])
        autotools.make()
        
        # remove some stuff that might get executed when added to path
        os.unlink('config.status')
        os.unlink('bootstrap')
        os.unlink('configure')

    def package(self):
        self.copy("*")
        
    def package_info(self):
        pkg = Path(self.package_folder)
        self.output.info("setting environment (pkg = %s)" % str(pkg))

        self.env_info.PATH.append(str(pkg))
